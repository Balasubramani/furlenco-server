'use strict';

/* Settings file for Application */

module.exports = {
	environment : 'local',
	db : {
		hostname : 'localhost',
		name : 'flurencodedb',
		username : '',
		password : '',
		replicaSet: '',
		connectionTimeout : 4000
	},
	port: 4500
};